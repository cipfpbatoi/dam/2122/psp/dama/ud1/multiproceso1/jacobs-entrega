package Ejercicio3;


import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        //Recoge la salida y la asigna a una variable
        Scanner sc = new Scanner(System.in);
        String aux = sc.nextLine();
        //Hasta que el usuario no introduce la palabra finalizar transformara el texto a minisculas y lo devolvera
        while(!aux.equals("finalizar")){
            System.out.println(aux.toLowerCase());
            aux = sc.nextLine();
        }
    }
}
