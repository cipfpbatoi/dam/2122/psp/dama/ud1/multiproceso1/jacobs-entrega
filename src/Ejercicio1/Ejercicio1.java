package Ejercicio1;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    private static final int TIEMPO_LIMITE = 2;
    public static void main(String[] args) {
        //Este if comprueba si han introducido suficientes argumentos
        if (args.length <= 0){
            System.out.println("Error, no hay paramtros suficientes");
            System.exit(-1);
        }
        ArrayList<String> list = new ArrayList<>();
        //Añade todos los argumentos en una lista
        for (int i = 0; i < args.length; i++){
            list.add(args[i]);
        }

        ProcessBuilder pb  = new ProcessBuilder(list);
        Process p = null;
        try{
            //Inicia el proceso hijo y espera 2 segundos como maximo a que este responde
            p = pb.start();
            if (!p.waitFor(TIEMPO_LIMITE, TimeUnit.SECONDS)){
                throw new InterruptedException();
            }
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error al inicializar el proceso hijo");
        } catch (InterruptedException e) {
            System.out.println("Tiempo agotado");
        }
        //Recoge la entrada del hijo
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String linea = null;
        String salida = null;
        //Si el proceso no ha dado un error entrara al if
        // Añade todo lo que ha devuelto a una cadena de texto
        if (p.exitValue() == 0) {
            try {
                while (((linea = reader.readLine()) != null)) {
                    salida += linea + "\n";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println(p.getErrorStream());
            System.exit(-1);
        }

        //Muestra el resultado por pantalla y añade todo el resultado al fichero
        System.out.println(salida);
        File f = new File("output.txt");
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            bw.write(salida);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
