package Ejercicio2;

import java.util.Random;
import java.util.Scanner;

public class Random10 {
    public static void main(String[] args) {
        //Recoge la entrada
        Scanner sc = new Scanner(System.in);
        //Genera un numero aleatorio mientras no introduzca la palabra stop
        while(!sc.nextLine().equals("stop")){
            System.out.println(new Random().nextInt(10));
        }
    }
}
