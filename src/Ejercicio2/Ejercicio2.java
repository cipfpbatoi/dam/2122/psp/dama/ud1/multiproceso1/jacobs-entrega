package Ejercicio2;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {
        String sisOp = System.getProperty("os.name").toLowerCase();
        String comando = "";

        //Comprueba que sistema operativo esta ejecutando el codigo para buscar el jar y añadirlo a un String
        if (sisOp.equals("windows 10")) {
            comando = "java -jar out\\artifacts\\Ejercicio2\\Ejercicio2.jar";
        } else {
            comando = "java -jar out/artifacts/Ejercicio2/Ejercicio2.jar";
        }

        //Separa el String por espacios
        List<String> argsList = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);
        String texto = "";
        String numero = "";
        File file = new File("randoms.txt");
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(file));

        try {
            //Inicia el subproceso
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner procesoSC = new Scanner(process.getInputStream());

            Scanner sc = new Scanner(System.in);

            String linea = sc.nextLine();
            //Mientras que el usuario no introduzca la palabra stop seguira pidiendo informacion
            while (!linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                //Recoge la salida y la muestra por pantalla
                texto = procesoSC.nextLine();
                System.out.println(texto);
                //Guarda en el archivo randoms.txt el numero random que se ha generado
                /**
                 * Si se utiliza un bufferedwritter, se debe de utilizar lo siguiente:
                 */
                bw2.write(texto);
                bw2.newLine();
                linea = sc.nextLine();
            }
            bw2.close();
        } catch (IOException ex) {
            Logger.getLogger(Ejercicio2.class.getName());
        }
    }
}
